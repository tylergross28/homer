module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{css,png,jpeg,html,js,json}"
  ],
  "swDest": "public/sw.js"
};